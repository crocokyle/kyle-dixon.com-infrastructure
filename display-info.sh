#!/bin/bash

# Get Vars
DASHBOARD_NODE=$(echo $(kubectl get pod -o=custom-columns=NAME:.metadata.name,STATUS:.status.phase,NODE:.spec.nodeName --all-namespaces | grep kubernetes-dashboard-) | cut -d ' ' -f 3)
DASHBOARD_PORT=$(echo $(kubectl get services -A | grep kubernetes-dashboard | grep NodePort) | cut -d ' ' -f 6 | cut -d ':' -f 2 | cut -d "/" -f 1)
DASHBOARD_SECRET=$(kubectl get secrets | grep dashboard-admin-sa | cut -d ' ' -f 1)
DASHBOARD_TOKEN=$(echo $(kubectl describe secret $DASHBOARD_SECRET | grep token:) | cut -d ' ' -f 2)
URL="https://${DASHBOARD_NODE}:${DASHBOARD_PORT}/#/login"

# Display important info
echo ""
echo ""
echo "Complete! You may now join worker nodes to the cluster using the following command:"
echo ""
kubeadm token create --print-join-command
echo ""
echo "Access the kubernetes dashboard at:"
echo ""
echo $URL
echo ""
echo "Using the token: "
echo ""
echo $DASHBOARD_TOKEN
echo ""
