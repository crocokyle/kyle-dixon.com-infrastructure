#!/bin/bash

# Clean up any old installations
echo ""
echo "Cleaning up any old clusters..."
kubectl delete cert flask-tls
sudo kubeadm reset -f
rm -r $HOME/.kube
sudo ifconfig cni0 down    
sudo ip link delete cni0

# Enable packet forwarding
sudo sysctl net.bridge.bridge-nf-call-iptables=1
TOKEN=$(sudo kubeadm token generate)

# Start up the cluster
echo ""
echo "Starting up the cluster..."
sudo kubeadm init --token=${TOKEN} --kubernetes-version=v1.21.2 --pod-network-cidr=10.244.0.0/16
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Allow control-plane to host pods
kubectl taint nodes --all node-role.kubernetes.io/master-

# Enable Kubernetes Dashboard UI
    # There is a bug with nodeport that allows access only from the node the pod is running on. 
    # See issue here: https://github.com/kubernetes/kubernetes/issues/58908
echo ""
echo "Installing kubernetes dashboard..."
kubectl apply -f https://gitlab.com/crocokyle/kyle-dixon.com-infrastructure/-/raw/master/manifests/kubernetes-dashboard.yaml
kubectl create serviceaccount dashboard-admin-sa
kubectl create clusterrolebinding dashboard-admin-sa --clusterrole=cluster-admin --serviceaccount=default:dashboard-admin-sa

# Install Flannel Networking
echo ""
echo "Installing Flannel Networking..."
kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml

# Install MetalLB
echo ""
echo "Installing MetalLB..."
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/metallb.yaml
kubectl apply -f https://gitlab.com/crocokyle/kyle-dixon.com-infrastructure/-/raw/master/manifests/metallb-config.yaml

# Install our webapp deployment and service
echo ""
echo "Applying flask-webapp manifest..."
kubectl apply -f https://gitlab.com/crocokyle/kyle-dixon.com-infrastructure/-/raw/master/manifests/flask-app.yaml

# Display dashboard secrets/URL
DASHBOARD_NODE=$(echo $(kubectl get pod -o=custom-columns=NAME:.metadata.name,STATUS:.status.phase,NODE:.spec.nodeName --all-namespaces | grep kubernetes-dashboard-) | cut -d ' ' -f 3)
DASHBOARD_PORT=$(echo $(kubectl get services -A | grep kubernetes-dashboard | grep NodePort) | cut -d ' ' -f 6 | cut -d ':' -f 2 | cut -d "/" -f 1)
DASHBOARD_SECRET=$(kubectl get secrets | grep dashboard-admin-sa | cut -d ' ' -f 1)
DASHBOARD_TOKEN=$(echo $(kubectl describe secret $DASHBOARD_SECRET | grep token:) | cut -d ' ' -f 2)

URL="https://${DASHBOARD_NODE}:${DASHBOARD_PORT}/#/login"
echo ""
echo "Access the kubernetes dashboard at:"
echo ""
echo $URL
echo ""
echo "Using the token: "
echo ""
echo $DASHBOARD_TOKEN
echo ""

# Install NGINX Ingress
echo ""
echo "Applying NGINX Ingress manifest..."
kubectl apply -f https://gitlab.com/crocokyle/kyle-dixon.com-infrastructure/-/raw/master/manifests/nginx-ingress.yaml

# Wait until Loadbalancer is created
echo ""
echo "Waiting for metalLB to generate an external IP for NGINX Ingress..."
echo "This can take up to 5 minutes."
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=300s

# Get LB IP
IP=$(kubectl get svc ingress-nginx-controller -n ingress-nginx -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
echo ""
echo "Please create a DNS A record and create port forwarding rules for port 80/443 on "$IP
read -p "Press ENTER to generate an SSL Certificate."

# Install Cert-Manager
echo ""
echo "Installing Cert-Manager..."
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.16.1/cert-manager.yaml
kubectl rollout status deployment cert-manager-webhook -n cert-manager

# Apply staging and production issuers
echo ""
echo "Applying staging and production certificate issuers..."
kubectl create -f https://gitlab.com/crocokyle/kyle-dixon.com-infrastructure/-/raw/master/manifests/staging-issuer.yaml
kubectl create -f https://gitlab.com/crocokyle/kyle-dixon.com-infrastructure/-/raw/master/manifests/prod-issuer.yaml

# Apply flask-ingress workaround
echo ""
echo "Disabling manifest validation due to a timeout bug..."
  # https://github.com/kubernetes/ingress-nginx/issues/5401
  # https://stackoverflow.com/questions/61365202/nginx-ingress-service-ingress-nginx-controller-admission-not-found
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission

# Apply flask-ingress config map
echo ""
echo "Generating production certificate..."
kubectl apply -f https://gitlab.com/crocokyle/kyle-dixon.com-infrastructure/-/raw/master/manifests/flask-ingress.yaml

# Display important info
echo ""
echo ""
echo "Complete! You may now join worker nodes to the cluster using the following command:"
echo ""
kubeadm token create --print-join-command
echo ""
echo "Access the kubernetes dashboard at:"
echo ""
echo $URL
echo ""
echo "Using the token: "
echo ""
echo $DASHBOARD_TOKEN
echo ""